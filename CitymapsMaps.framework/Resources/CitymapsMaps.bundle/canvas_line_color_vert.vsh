attribute vec2 a_position;
attribute vec2 a_perp;
attribute vec2 a_texCoord;
attribute float a_segmentLength;

uniform mat4 u_mvp;
uniform float u_width;
uniform highp float u_resolution;
uniform highp float u_aaPadding;

varying vec2 v_texCoord;
varying mediump float v_distance;
varying mediump float v_min;
varying mediump float v_smoothing;

void main()
{
    v_texCoord = a_texCoord;
    
    highp vec2 finalVector = a_perp * (u_width * u_resolution);
    finalVector += a_perp * (u_aaPadding * u_resolution);
    
    highp vec2 v2Pos = a_position + finalVector;
    
    v_min = u_width / (u_width + u_aaPadding);
    
    // Map 0..1 to -1..1
    v_distance = a_texCoord.y * 2.0 - 1.0;
    
    v_smoothing = 1.0 / (u_aaPadding + 4.0 * u_width);
    
    gl_Position = u_mvp * vec4(v2Pos.x, v2Pos.y, 0.0, 1.0);
}