attribute highp vec2 a_position;
attribute lowp vec4 a_color;
attribute lowp vec4 a_texCoordSizePadding;
//attribute lowp float a_size;

varying lowp vec4 v_color;
varying lowp vec2 v_texCoord;
varying lowp float v_size;

uniform mat4 u_mvp;
uniform vec2 u_origin;
uniform highp float u_resolution;

void main()
{
    v_color = a_color;
    v_texCoord = a_texCoordSizePadding.xy;
    float size = (a_texCoordSizePadding.z * 255.0) * 0.25;
    v_size = size;

    vec2 position = a_position + ((a_texCoordSizePadding.xy - vec2(0.5, 0.5)) * (u_resolution * size));

    highp mat4 modelMatrix = mat4(
                                  1.0, 0.0, 0.0, 0.0,
                                  0.0, 1.0, 0.0, 0.0,
                                  0.0, 0.0, 1.0, 0.0,
                                  u_origin.x, u_origin.y, 0.0, 1.0
                                  );
    
    gl_Position = (u_mvp * modelMatrix) * vec4(position, 0.0, 1.0);
}