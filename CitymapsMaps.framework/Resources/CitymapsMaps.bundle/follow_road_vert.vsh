attribute vec2 a_anchorPos;
attribute vec2 a_pos;
attribute vec2 a_texCoord;
attribute vec4 a_color;
attribute float a_angle;

uniform mat4 u_mvp;

varying mediump vec2 v_texCoord;
varying mediump vec4 v_color;

void main()
{
	v_texCoord = a_texCoord;
    v_color = a_color;
    
    float sinA = sin(a_angle);
    float cosA = cos(a_angle);
    
    vec2 rotatedPos;
    rotatedPos.x = (cosA * a_pos.x) - (sinA * a_pos.y);
    rotatedPos.y = (sinA * a_pos.x) + (cosA * a_pos.y);
    
    vec2 position = a_anchorPos + rotatedPos;
	gl_Position = u_mvp * vec4(position, 0.0, 1.0);
}
