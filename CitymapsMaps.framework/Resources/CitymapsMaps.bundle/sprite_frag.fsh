precision lowp float;

varying vec2 v_texCoord;

uniform sampler2D u_texture;
uniform float u_alpha;
uniform vec4 u_blendColor;

void main()
{
    vec4 color = texture2D(u_texture, v_texCoord);
    color *= u_alpha;
    color *= u_blendColor;
    gl_FragColor = color;
}

