attribute vec4 a_posAndPerp;
attribute vec2 a_layerAndU;

varying vec4 v_fillColor;
varying vec4 v_strokeColor;

varying float v_u;
varying float v_strokeFillBorderU;

uniform mat4 u_mvp;
uniform vec4 u_colors[75];
uniform vec4 u_outlineColors[75];
uniform vec2 u_widths[75];

void main()
{
    int layerIdInt = int(a_layerAndU.x);
    
    float fillWidth = u_widths[layerIdInt].x;
    float strokeWidth = u_widths[layerIdInt].y;
    v_fillColor = u_colors[layerIdInt];
    v_strokeColor = u_outlineColors[layerIdInt];
    
    float width = fillWidth + strokeWidth;
    
    v_strokeFillBorderU = strokeWidth / width;
    
    // Map a_u (0..1) to (-1..1)
    v_u = 2.0 * a_layerAndU.y - 1.0;
    vec2 v2Pos = a_posAndPerp.xy + (a_posAndPerp.zw * width);
    gl_Position = u_mvp * vec4(v2Pos.x, v2Pos.y, 0.0, 1.0);
}


