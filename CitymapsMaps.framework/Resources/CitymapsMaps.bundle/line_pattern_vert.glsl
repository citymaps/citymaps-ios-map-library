attribute highp vec2 a_position;
attribute lowp vec4 a_color;
attribute mediump vec4 a_perpSignSize;
attribute lowp vec4 a_linePattern;
attribute highp float a_length;

varying lowp vec4 v_color;
varying highp float v_distance;
varying mediump float v_min;
varying mediump float v_smoothing;
varying highp float v_segmentLength;
varying lowp vec4 v_linePattern;

uniform mat4 u_mvp;
uniform vec2 u_origin;
uniform highp float u_resolution;
uniform highp float u_baseResolution;
uniform highp float u_aaPadding;

const float MAX_LINE_SIZE = (32767.0 / 255.0);


void main()
{
    lowp vec2 perp = a_perpSignSize.xy * 2.0;
    lowp float distSign = a_perpSignSize.z;
    float size = a_perpSignSize.w * MAX_LINE_SIZE;

    highp vec2 finalVector = perp * (size * u_resolution);
    finalVector += perp * 2.0 * u_aaPadding;

    highp vec2 v2Pos = a_position + finalVector;

    v_min = size / (size + 2.0);
    v_color = a_color;
    v_distance = distSign;
    v_smoothing = 1.0 / (2.0 + 4.0 * size);
    v_segmentLength = a_length;
    v_linePattern = a_linePattern * u_baseResolution;

    highp mat4 modelMatrix = mat4(
                                  1.0, 0.0, 0.0, 0.0,
                                  0.0, 1.0, 0.0, 0.0,
                                  0.0, 0.0, 1.0, 0.0,
                                  u_origin.x, u_origin.y, 0.0, 1.0
                                  );
    
    gl_Position = (u_mvp * modelMatrix) * vec4(v2Pos.x, v2Pos.y, 0.0, 1.0);
}
