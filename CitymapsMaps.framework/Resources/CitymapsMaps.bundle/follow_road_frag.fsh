precision mediump float;

uniform sampler2D u_glyphAtlasTexture;

varying mediump vec2 v_texCoord;
varying mediump vec4 v_color;
void main()
{
	float alpha = texture2D(u_glyphAtlasTexture, v_texCoord).a * v_color.a;
    gl_FragColor = vec4(v_color.xyz * alpha, alpha);
}