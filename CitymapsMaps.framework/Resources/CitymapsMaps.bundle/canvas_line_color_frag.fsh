precision mediump float;

varying vec2 v_texCoord;
varying mediump float v_distance;
varying mediump float v_min;
varying mediump float v_smoothing;

uniform vec4 u_color;

const vec4 ClearColor = vec4(0.0, 0.0, 0.0, 0.0);

void main()
{
    mediump float distance = abs(v_distance);
    lowp vec4 color = u_color;
    color.a *= smoothstep(v_min + v_smoothing, v_min - v_smoothing, distance);
    
    gl_FragColor = color;
}
