precision lowp float;

varying lowp vec4 v_color;
varying lowp vec2 v_texCoord;
varying lowp float v_size;

void main()
{
    mediump float distance = distance(v_texCoord, vec2(0.5, 0.5));
    lowp vec4 color = v_color;
    float maxDistance = 0.5;
    float minDistance = maxDistance - (0.1 * (1.0 / v_size));
    color.a = 1.0 - smoothstep(minDistance, maxDistance, distance);

    gl_FragColor = color;
    //gl_FragColor = vec4(distance, distance, distance, 1.0);
}