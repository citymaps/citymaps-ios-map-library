attribute highp vec2 a_position;
attribute vec4 a_color;

varying vec4 v_color;
varying highp vec2 v_texCoord;
varying float v_isLandCover;

uniform mat4 u_mvp;
uniform vec2 u_origin;
uniform float u_baseResolution;
uniform float u_tileSize;
uniform vec4 u_landColor;

void main()
{
    v_color = a_color;
    v_texCoord = (a_position / u_baseResolution / u_tileSize);
    v_isLandCover = u_landColor == a_color ? 1.0 : 0.0;
    
    highp mat4 modelMatrix = mat4(1.0, 0.0, 0.0, 0.0,
                                  0.0, 1.0, 0.0, 0.0,
                                  0.0, 0.0, 1.0, 0.0,
                                  u_origin.x, u_origin.y, 0.0, 1.0);
    
    gl_Position = (u_mvp * modelMatrix) * vec4(a_position.x, a_position.y, 0.0, 1.0);
}
