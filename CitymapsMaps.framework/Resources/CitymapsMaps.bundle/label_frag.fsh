precision mediump float;

uniform sampler2D u_glyphAtlasTexture;
uniform float u_alpha;
uniform vec4 u_color;

varying mediump vec2 v_texCoord;

void main()
{
	float alpha = texture2D(u_glyphAtlasTexture, v_texCoord).a * u_alpha * u_color.a;
    gl_FragColor = vec4(u_color.xyz, alpha);
}