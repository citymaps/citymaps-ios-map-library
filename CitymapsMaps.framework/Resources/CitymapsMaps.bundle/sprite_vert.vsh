attribute vec2 a_coords;

varying vec2 v_texCoord;

uniform mat4 u_mvp;
uniform vec4 u_dimensions;
uniform vec4 u_textureDimensions;
uniform vec2 u_anchorPoint;
uniform vec2 u_rotation;

void main()
{
    vec2 modelCoords = a_coords * u_dimensions.zw;
    modelCoords -= u_anchorPoint.xy * u_dimensions.zw;
    
	vec2 baseCoords;
    baseCoords.x = (modelCoords[0] * u_rotation[1]) - (modelCoords[1] * u_rotation[0]);
    baseCoords.y = (modelCoords[0] * u_rotation[0]) + (modelCoords[1] * u_rotation[1]);

    baseCoords += u_dimensions.xy;
    
    v_texCoord = u_textureDimensions.xy + (a_coords * u_textureDimensions.zw);
    
    gl_Position = u_mvp * vec4(baseCoords, 0.0, 1.0);
}
