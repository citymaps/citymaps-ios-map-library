attribute vec2 a_position;
attribute vec4 a_color;
attribute float a_textureId;

varying vec4 v_color;
varying vec4 v_texDimensions;
varying vec2 v_position;
varying vec2 v_texCoord;

uniform mat4 u_mvp;
uniform vec2 u_origin;
uniform vec4 u_texDimensions[16];
uniform float u_baseResolution;
uniform float u_tileSize;

void main()
{
    v_color = a_color;
    v_position = a_position;
    v_texDimensions = u_texDimensions[int(a_textureId)];
    v_texCoord = (a_position / u_baseResolution / u_tileSize);
    // flip y for correct uv orientation.
    v_texCoord.y *= -1.0;
    
    highp mat4 modelMatrix = mat4(
                                  1.0, 0.0, 0.0, 0.0,
                                  0.0, 1.0, 0.0, 0.0,
                                  0.0, 0.0, 1.0, 0.0,
                                  u_origin.x, u_origin.y, 0.0, 1.0
                                  );
    gl_Position = (u_mvp * modelMatrix) * vec4(a_position.x, a_position.y, 0.0, 1.0);
}