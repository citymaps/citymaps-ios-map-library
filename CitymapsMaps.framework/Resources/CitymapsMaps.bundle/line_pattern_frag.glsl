precision highp float;

varying lowp vec4 v_color;
varying highp float v_distance;
varying mediump float v_min;
varying mediump float v_smoothing;
varying highp float v_segmentLength;
varying highp vec4 v_linePattern;

void main()
{
    float patternLength = v_linePattern[0] + v_linePattern[1] + v_linePattern[2] + v_linePattern[3];

    float patternPosition = mod(v_segmentLength, patternLength);

    float min1 = 0.0;
    float max1 = v_linePattern[0];

    float min2 = v_linePattern[0] + v_linePattern[1];
    float max2 = min2 + v_linePattern[2];

    float killPixel = float((patternPosition >= min1 && patternPosition <= max1) ||
    (patternPosition >= min2 && patternPosition <= max2));
    
    highp float distance = abs(v_distance);
    lowp vec4 color = v_color;
    color.a *= smoothstep(v_min + v_smoothing, v_min - v_smoothing, distance);

    gl_FragColor = color * killPixel;
}
