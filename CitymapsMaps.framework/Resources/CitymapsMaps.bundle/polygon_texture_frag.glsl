precision highp float;

varying vec2 v_position;
varying vec4 v_color;
varying vec4 v_texDimensions;
varying vec2 v_texCoord;

uniform sampler2D u_texture;

void main()
{
    vec2 fraction = fract(v_texCoord);
    vec2 finalTexCoord = v_texDimensions.xy + fraction * v_texDimensions.zw;
    vec4 color = texture2D(u_texture, finalTexCoord);
    color *= v_color;
    gl_FragColor = color;
}