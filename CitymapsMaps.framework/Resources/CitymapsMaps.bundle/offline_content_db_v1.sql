CREATE TABLE IF NOT EXISTS comment (
    comment_id TEXT,
    object_id TEXT,
    created_at INTEGER,
    user_id TEXT,
    note_text TEXT,
    business_lat REAL,
    business_lon REAL,
    UNIQUE (comment_id) ON CONFLICT REPLACE
);

CREATE INDEX IF NOT EXISTS comment_comment_id ON comment (comment_id);

CREATE TABLE IF NOT EXISTS marker (
    marker_id TEXT,
    map_id TEXT,
    business_id TEXT,
    owner_id TEXT,
    map_name TEXT,
    created_at INTEGER,
    note_text TEXT,
    business_lat REAL,
    business_lon REAL,
    UNIQUE (marker_id) ON CONFLICT REPLACE
);

CREATE INDEX IF NOT EXISTS marker_marker_id ON marker (marker_id);

CREATE TABLE IF NOT EXISTS like (
    like_id TEXT,
    user_id TEXT,
    object_id TEXT,
    business_lat REAL,
    business_lon REAL,
    UNIQUE (like_id) ON CONFLICT IGNORE
);

CREATE INDEX IF NOT EXISTS like_like_id ON like (like_id);
CREATE INDEX IF NOT EXISTS like_user_id ON like (user_id);
CREATE INDEX IF NOT EXISTS like_object_id ON like (object_id);

CREATE TABLE IF NOT EXISTS user (
    user_id TEXT,
    username TEXT,
    first_name TEXT,
    last_name TEXT,
    UNIQUE (user_id) ON CONFLICT REPLACE
);

CREATE INDEX IF NOT EXISTS user_user_id ON user (user_id);

CREATE TABLE IF NOT EXISTS content_refs (
    owner_id TEXT,
    object_id TEXT,
    object_type TEXT,
    UNIQUE (owner_id, object_id, object_type) ON CONFLICT IGNORE
);

CREATE INDEX IF NOT EXISTS content_refs_owner_id ON content_refs (owner_id);