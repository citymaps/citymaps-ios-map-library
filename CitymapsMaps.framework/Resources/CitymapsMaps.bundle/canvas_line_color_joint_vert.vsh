attribute vec2 a_position;
attribute vec2 a_texCoord;

uniform mat4 u_mvp;
uniform float u_width;
uniform highp float u_resolution;

varying lowp vec2 v_texCoord;
varying lowp float v_size;

void main()
{
    v_texCoord = a_texCoord;
    v_size = u_width;
    
    vec2 position = a_position + ((a_texCoord - vec2(0.5, 0.5)) * (u_resolution * u_width * 2.0));
    
    gl_Position = u_mvp * vec4(position, 0.0, 1.0);
}