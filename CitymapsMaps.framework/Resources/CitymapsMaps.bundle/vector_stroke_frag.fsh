precision mediump float;

varying vec4 v_fillColor;
varying vec4 v_strokeColor;

varying float v_u;
varying float v_strokeFillBorderU;

void main()
{
    // ( -1..0..1) --> ( 1..0..1)
    float u = abs(v_u);
    
    // ( 1..0..1) --> ( 0..1..0)
    float oneMinusU = 1.0 - u;
    float innerBlendBorder = 1.5 * v_strokeFillBorderU;
    
    if (oneMinusU < v_strokeFillBorderU)
    {
        vec4 color = v_strokeColor;
        color.a *= 0.5 + (oneMinusU / v_strokeFillBorderU);
        gl_FragColor = color;
    }
    else
    {
        float alpha = clamp(0.75, 1.0, oneMinusU / innerBlendBorder);
        gl_FragColor = v_fillColor * alpha + v_strokeColor * (1.0 - alpha);
    }
}
