CREATE TABLE IF NOT EXISTS package (
    package_id TEXT,
    created_at INTEGER,
    updated_at INTEGER,
    available INTEGER,
    name TEXT,
    disk_size INTEGER,
    download_size INTEGER,
    file_count INTEGER,
    lon_min REAL,
    lon_max REAL,
    lat_min REAL,
    lat_max REAL,
    installed_at INTEGER,
    download_url TEXT,
    version INTEGER,
    state TEXT,
    country TEXT,
    region_id TEXT,
    UNIQUE(package_id) ON CONFLICT REPLACE
);

CREATE INDEX IF NOT EXISTS package_package_id ON package (package_id);
CREATE INDEX IF NOT EXISTS package_region_id ON package (region_id);

CREATE TABLE IF NOT EXISTS region (
    region_id TEXT,
    name TEXT,
    center_lon REAL,
    center_lat REAL,
    bounds_left REAL,
    bounds_right REAL,
    bounds_bottom REAL,
    bounds_top REAL,
    snippet TEXT,
    state_code TEXT,
    country_code TEXT,
    slug TEXT,
    population INTEGER,
    UNIQUE (region_id) ON CONFLICT REPLACE
);

CREATE INDEX IF NOT EXISTS region_region_id ON region (region_id);

CREATE TABLE IF NOT EXISTS file (
   file_id TEXT,
   data BLOB,
   version INTEGER,
   UNIQUE(file_id) ON CONFLICT REPLACE
);

CREATE INDEX IF NOT EXISTS file_file_id ON file (file_id);

CREATE TABLE IF NOT EXISTS file_package (
   file_id TEXT,
   package_id TEXT
);
