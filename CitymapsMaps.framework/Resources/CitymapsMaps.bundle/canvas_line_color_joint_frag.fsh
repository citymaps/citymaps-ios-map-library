precision lowp float;

varying lowp vec2 v_texCoord;
varying lowp float v_size;

uniform vec4 u_color;

void main()
{
    mediump float distance = distance(v_texCoord, vec2(0.5, 0.5));
    vec4 color = u_color;
    float maxDistance = 0.5;
    float minDistance = maxDistance - (0.1 * (1.0 / v_size));
    color.a *= 1.0 - smoothstep(minDistance, maxDistance, distance);
    
    gl_FragColor = color;
}