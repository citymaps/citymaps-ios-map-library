precision mediump float;

varying vec2 v_texCoord;
varying mediump float v_distance;
varying mediump float v_min;
varying mediump float v_smoothing;
varying highp float v_segmentLength;

uniform vec4 u_color;
uniform vec4 u_linePattern;

void main()
{
    float patternLength = u_linePattern[0] + u_linePattern[1] + u_linePattern[2] + u_linePattern[3];
    
    float patternPosition = mod(v_segmentLength, patternLength);
    
    float min1 = 0.0;
    float max1 = u_linePattern[0];
    
    float min2 = u_linePattern[0] + u_linePattern[1];
    float max2 = min2 + u_linePattern[2];
    
    float killPixel = float((patternPosition >= min1 && patternPosition <= max1) ||
                            (patternPosition >= min2 && patternPosition <= max2));
    
    mediump float distance = abs(v_distance);
    lowp vec4 color = u_color;
    color.a *= smoothstep(v_min + v_smoothing, v_min - v_smoothing, distance);
    
    gl_FragColor = color * killPixel;
}
