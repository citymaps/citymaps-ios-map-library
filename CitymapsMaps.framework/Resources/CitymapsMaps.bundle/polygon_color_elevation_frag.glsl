precision highp float;

varying vec4 v_color;
varying highp vec2 v_texCoord;
varying float v_isLandCover;

uniform sampler2D u_elevationTexture;
uniform float u_hasElevation;
uniform float u_zenithAngle;
uniform float u_azimuthAngle;
uniform float u_zFactor;
uniform float u_hillshadeFactor;
uniform float u_colorFactor;
uniform float u_pixelSize;
uniform float u_landFactor;

const float PI = 3.14159265359;
const float DBL_PI = 2.0 * PI;
const float HALF_PI = PI / 2.0;
const vec3 LAND_COVER_BACKUP = vec3(187.0 / 255.0,218.0 / 255.0,205.0 / 255.0);
const float HILLSHADE_OFFSET = (87.0 / 255.0);
const float EPSILON = 0.00001;

void main()
{
    vec2 texCoord = vec2(v_texCoord.x, 1.0 - v_texCoord.y);
    if (texCoord.y < EPSILON || texCoord.y > 1.0 - EPSILON) {
        discard;
    }
    
    vec4 e = texture2D(u_elevationTexture, texCoord);
    
    float backupStep = step(0.01, e.r + e.g + e.b);
    vec4 landCoverColor = backupStep * vec4(e.rgb, 1.0) + (1.0 - backupStep) * vec4(LAND_COVER_BACKUP, 1.0);
    
    // v_isLandCover == 0  --> 1;
    // v_isLandColor == 1 --> 1.0 - u_landFactor;
    float solidColorFactor = (1.0 - v_isLandCover) + v_isLandCover * (1.0 - u_landFactor);
    float landColorFactor = v_isLandCover * u_landFactor;

    vec4 finalColor = (landColorFactor * landCoverColor) + (solidColorFactor * v_color);
    
    float a = texture2D(u_elevationTexture, texCoord + vec2(-u_pixelSize, u_pixelSize)).a;
    float b = texture2D(u_elevationTexture, texCoord + vec2(0.0, u_pixelSize)).a;
    float c = texture2D(u_elevationTexture, texCoord + vec2(u_pixelSize, u_pixelSize)).a;
    float d = texture2D(u_elevationTexture, texCoord + vec2(-u_pixelSize, 0.0)).a;
    float f = texture2D(u_elevationTexture, texCoord + vec2(u_pixelSize, 0.0)).a;
    float g = texture2D(u_elevationTexture, texCoord + vec2(-u_pixelSize, -u_pixelSize)).a;
    float h = texture2D(u_elevationTexture, texCoord + vec2(0.0, -u_pixelSize)).a;
    float i = texture2D(u_elevationTexture, texCoord + vec2(u_pixelSize, -u_pixelSize)).a;
    
    float dzdx = ((c + (2.0 * f) + i) - (a + (2.0 * d) + g)) / 8.0;
    float dzdy = ((g + (2.0 * h) + i) - (a + (2.0 * b) + c)) / 8.0;
    
    // Approx
//    float b = texture2D(u_elevationTexture, texCoord + vec2(0.0, u_pixelSize)).a;
//    float d = texture2D(u_elevationTexture, texCoord + vec2(-u_pixelSize, 0.0)).a;
//    float f = texture2D(u_elevationTexture, texCoord + vec2(u_pixelSize, 0.0)).a;
//    float h = texture2D(u_elevationTexture, texCoord + vec2(0.0, -u_pixelSize)).a;
//    
//    float dzdx = (f - d) / 2.0;
//    float dzdy = (h - b) / 2.0;
    
    float slopeAngle = atan(u_zFactor * sqrt((dzdx * dzdx) + (dzdy * dzdy)));
    
    float aspect = 0.0;
    if (dzdx != 0.0) {
        aspect = atan(dzdy, -dzdx);
        
        if (aspect < 0.0) {
            aspect = (DBL_PI) + aspect;
        }
    } else {
        if (dzdy > 0.0) {
            aspect = HALF_PI;
        } else if (dzdy < 0.0) {
            aspect = (DBL_PI) - (HALF_PI);
        }
    }
    
    float hillshade = ((cos(u_zenithAngle) * cos(slopeAngle)) +
                       (sin(u_zenithAngle) * sin(slopeAngle) * cos(u_azimuthAngle - aspect))) - HILLSHADE_OFFSET;
    
    finalColor = mix(finalColor, finalColor * u_colorFactor, hillshade * u_hillshadeFactor);
    finalColor.a = 1.0;
    
    gl_FragColor = finalColor;
}
