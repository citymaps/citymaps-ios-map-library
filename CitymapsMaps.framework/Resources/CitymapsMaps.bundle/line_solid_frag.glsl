precision lowp float;

varying lowp vec4 v_color;
varying mediump float v_distance;
varying mediump float v_min;
varying mediump float v_smoothing;

void main()
{
    mediump float distance = abs(v_distance);
    lowp vec4 color = v_color;
    color.a *= smoothstep(v_min + v_smoothing, v_min - v_smoothing, distance);
    
    gl_FragColor = color;
    //gl_FragColor = vec4(distance, distance, distance, 1.0);
}