//
//  CEEllipse.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CEMapTypes.h"
#import "CEFeature.h"

/** This class represents a user defined ellipse to be placed on a CECanvasLayer. */
@interface CEEllipse : CEFeature

/**
 @name Initializers
 */

/** Initializes a new ellipse with the given position and radii.
 @param position The center of the ellipse.
 @param radii The x and y radii of the ellipse.
 @return A new ellipse described by the given parameters.
 */
 - (id)initWithPosition:(CLLocationCoordinate2D)position andRadii:(CGSize)radii;

/**
 @name Properties
 */

/**
 The center of the ellipse.
 */
@property (nonatomic, assign) CLLocationCoordinate2D position;

/**
 The x and y radii of the ellipse, defined in LonLat coordinates.
 */
@property (nonatomic, assign) CGSize radii;

@end
