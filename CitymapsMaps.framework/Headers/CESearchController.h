//
//  CESearchController.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 12/17/15.
//  Copyright © 2015 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CESearchQuery.h"
#import "CESearchResult.h"
#import "CEBusinessData.h"

typedef void(^SearchResultsCallback)(NSError *, NSArray<CESearchResult *> *);
typedef void(^SearchBusinessResultsCallback)(NSError *, NSArray<CEBusinessData *> *);

@interface CESearchController : NSObject

+ (void)autocompleteQuery:(CESearchQuery *)query callback:(SearchResultsCallback)callback;
+ (void)searchQuery:(CESearchQuery *)query callback:(SearchResultsCallback)callback;
+ (void)topPlacesQuery:(CLLocationCoordinate2D)location limit:(NSInteger)limit offset:(NSInteger)offset callback:(SearchBusinessResultsCallback)callback;
+ (void)guideCategoryQuery:(NSInteger)categoryId regionId:(NSString *)regionId callback:(SearchBusinessResultsCallback)callback;
@end
