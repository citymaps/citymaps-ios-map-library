#import <Foundation/Foundation.h>
#import "CEDataSource.h"

@interface CEWebDataSourceOptions : CEDataSourceOptions

- (id) initWithURL:(NSString *)url;
@property (nonatomic, strong) NSString *url;

@end

/** A data source which receives its data from an external web address.  
 
 The web address is determined using a token replacement techinque.  The tokens are described as follows:
 
 - {x} - The mercator x-coordinate of the tile
 - {y} - The mercator y-coordinate of the tile
 - {zoom} - The zoom level of the tile
 
 An example URL:
 
 - http://www.example.com/tiles/{zoom}/{x}/{y}.png
 
 The following options are available at construction:

 - url - The tokenized tile URL for the data source.
 
 See CEDataSource for more information about data sources in general, and for additional avaiable options.
 */

@interface CEWebDataSource : CEDataSource

- (id)initWithOptions:(CEWebDataSourceOptions *)options;
- (id)initWithURL:(NSString *)url;

@property (strong, nonatomic) NSString* url;

@end
