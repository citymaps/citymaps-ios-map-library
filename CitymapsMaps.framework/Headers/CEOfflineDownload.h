//
//  CEOfflineDownloadRequest.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 5/11/15.
//  Copyright (c) 2015 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEMapTypes.h"

typedef NS_ENUM(NSInteger, CEOfflineDownloadError) {
    CEOfflineDownloadErrorOK = 0,
    CEOfflineDownloadErrorNetwork = 1,
    CEOfflineDownloadErrorInstall = 2,
    CEOfflineDownloadErrorCancelled = 3
};

typedef NS_ENUM(NSInteger, CEOfflineDownloadState) {
    CEOfflineDownloadStateDownloading = 0,
    CEOfflineDownloadStateInstalling = 1
};

@interface CEOfflineDownloadProgress : NSObject

@property (nonatomic, assign) CEOfflineDownloadState state;
@property (nonatomic, assign) NSUInteger totalDiskSize;
@property (nonatomic, assign) NSUInteger totalDownloadSize;
@property (nonatomic, assign) NSUInteger totalFileCount;
@property (nonatomic, assign) NSUInteger finishedBytes;
@property (nonatomic, assign) NSUInteger finishedFiles;
@end

@interface CEOfflineDownload : NSObject
- (void)cancel;
- (void)pause:(nullable void(^)(BOOL canResume))callback;
- (void)resume;

@end

@protocol CEOfflineDownloadDelegate <NSObject>

/** Called when the download request has been started.*/
- (void)offlineDownload:(CEOfflineDownload* _Nonnull)download didStart:(CEOfflineDownloadProgress * _Nonnull)progress;

/** Called periodically as the offline package is downloaded.*/
- (void)offlineDownload:(CEOfflineDownload* _Nonnull)download didUpdate:(CEOfflineDownloadProgress * _Nonnull)progress;

/** Called when the download request has finished downloading the offline package.*/
- (void)offlineDownloadDidComplete:(CEOfflineDownload * _Nonnull)download;

/** Called at any point when some network error occurs and the download could not be completed. May be called upon cancellation as well.*/
- (void)offlineDownload:(CEOfflineDownload * _Nullable)download didFailWithError:(NSError * _Nullable)error;

@end

@interface CEPackageFunFact : NSObject

@property (nonnull, strong, nonatomic) NSString *header;
@property (nonnull, strong, nonatomic) NSString *body;

@end

@interface CEOfflineRegion : NSObject

/** ID for the region */
@property (nonnull, strong, nonatomic) NSString *regionId;

/** Name of the region */
@property (nullable, strong, nonatomic) NSString *name;

/** Two letter state code */
@property (nullable, strong, nonatomic) NSString *stateCode;

/** Two letter country code */
@property (nullable, strong, nonatomic) NSString *countryCode;

/** Center point in Lon/Lat coordinates */
@property (assign, nonatomic) CLLocationCoordinate2D center;

/** Boundaries in Lon/Lat coordinates */
@property (strong, nonatomic) CECoordinateBounds *boundingBox;

/** Population */
@property (assign, nonatomic) NSInteger population;

/** A snippet of plain text from the Wikitravel article */
@property (nullable, strong, nonatomic) NSString *snippet;

/** Slug URL */
@property (nullable, strong, nonatomic) NSString *slug;

/** Number of offline packages */
@property (assign, nonatomic) NSInteger offlinePackageCount;


@end

@interface CEOfflinePackage : NSObject

/**
 * @name Properties
 *
 */
/** The identifier of the package. This is the value used to download a package. */
@property (nonnull, nonatomic, strong) NSString *packageId;

/** Version of the package */
@property (nonatomic, assign) NSInteger version;

/** The date the package was created. */
@property (nonatomic, assign) NSTimeInterval createdAt;

/** The date the package was last updated. */
@property (nonatomic, assign) NSTimeInterval updatedAt;

/** The date the package was installed */
@property (nonatomic, assign) NSTimeInterval installedAt;

/** Whether the package is available for download. False means that the package has not yet finished being generated. */
@property (nonatomic, assign) BOOL available;

/** Name of the city */
@property (nonnull, nonatomic, strong) NSString *name;

/** State or Province of the city */
@property (nonnull, nonatomic, strong) NSString *state;

/** Country of the city */
@property (nonnull, nonatomic, strong) NSString *country;

/** Size of package on disk */
@property (nonatomic, assign) NSInteger diskSize;

/** Size of package when download */
@property (nonatomic, assign) NSInteger downloadSize;

/** Number of files included in the package */
@property (nonatomic, assign) NSInteger fileCount;

/** The bounds of the package bounding box. */
@property (nonnull, nonatomic, strong) CECoordinateBounds *bounds;

/** Whether this package is currently installed on the device */
@property (nonatomic, assign) BOOL installed;

/** Fun facts for this city */
@property (nonnull, nonatomic, strong) NSArray<CEPackageFunFact *>* funFacts;

/** Download URL */
@property (nullable, nonatomic, strong) NSString *downloadUrl;

/** The region ID of this package */
@property (nullable, nonatomic, strong) NSString *regionId;

/** The region object */
@property (nullable, nonatomic, strong) CEOfflineRegion *region;

@end



