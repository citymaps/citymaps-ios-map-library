//
//  CELine.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CEMapTypes.h"
#import "CEFeature.h"

/** This class represents a user defined line to be placed on a CECanvasLayer. */
@interface CELine : CEFeature

/**
 * @name Initializers
 */

/** Initializes a new line. */
- (id)init;

/* Initializes a new line with the given points */
- (id)initWithPoints:(CLLocationCoordinate2D*)points count:(int)count;

/**
 * @name Points
 */

/** This method adds a point to the line.
 * @param point The point to add to the line.
 */
- (void)addPoint:(CLLocationCoordinate2D)point;

/** This method adds a set of points to the line.
 * @param points Points to add to the line.
 * @param count number of points to add.
 */
- (void)addPoints:(CLLocationCoordinate2D*)points count:(int)count;

/** This method returns the point at the given index.
 * @param index The index of the point you want to retrieve.
 * @return The point requested.
 */
- (CLLocationCoordinate2D)pointAtIndex:(int)index;

/** This method returns the number of points in the line.
 * @return The number of points in the line.
 */
- (NSInteger)numPoints;

/**
 * @name Properties
 */

/** The fill width of the line (in pixels). */
@property (nonatomic, assign) float width;

/** A repeating texture */
@property (nonatomic, strong) UIImage *texture;

/** How often to repeat the texture, in pixels */
@property (nonatomic, assign) float textureRepeat;

/** 
 Dash pattern string.  Represented by a string of comma separated values.  Must come in pairs.  Currently only 2 pairs supported maximum 
 
 1-pair example: "6,8".  This will have 6 pixels on, 8 pixels off, repeating
 2-pair example: "6,8,2,3".  This will have 6 pixels on, then 8 pixels off, then 2 pixels on, then 3 pixels off, repeating
 */
@property (nonatomic, assign) NSString *linePattern;

@property (nonatomic, assign) BOOL geodesic;

@end
