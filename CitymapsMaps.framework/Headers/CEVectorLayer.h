#import "CETileLayer.h"
#import "CERoutingTypes.h"

@class CERoutingRequest;
@class CERoute;
@class CERouteState;
@protocol CENavigationDelegate <NSObject>

@required

//- (void)navigationDidReroute:(CERoute *)route;
//- (void)navigationDidFinish:(BOOL)reachedDestination;

/** Called when the user should be notified of an instruction, either via voice or text. */
- (void)navigationDidSendNotification:(NSString *)notification;

/** Called when routing has started a new instruction. */
- (void)navigationDidBeginInstruction:(NSInteger)instruction;

/** Called periodically to update the overall state of routing. */
- (void)navigationDidUpdate:(CERouteState *)state;
@end

@interface CEVectorLayerOptions : CETileLayerOptions

@property (assign, nonatomic) BOOL backgroundEnabled;

@end

/** This is the base layer used by the Citymaps map view to draw the vector-based data received from the Citymaps tile server.
 
 See CECitymapsTileLayer for a list of available options.
 
 */

@interface CEVectorLayer : CETileLayer

/**
 @name Initializers
 */

- (id)initWithDefaultOptions;

- (void)startNavigationForRoute:(CERoute *)route;

- (void)setElevationEnabled:(BOOL)enabled;

@property (nonatomic, weak) id<CENavigationDelegate> navigationDelegate;

@end
