//
//  CEMarker.h
//  vectormap2
//
//  Created by Adam Eskreis on 7/24/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import "CEMapTypes.h"

@class CELabel;
@class CEMarker;
@class CEMarkerAttachment;
@class CEMarkerAttachmentView;
@class CEMapView;

/** 
 A delegate to notify an object when a marker receives user interaction.
 */

@protocol CEMarkerDelegate <NSObject>

@optional
/** Notifies the delegate when a marker the user touches up on a marker.
 
 @param marker The marker which was touched.
 @return Whether the event has been consumed.
 */
- (BOOL)markerTouchUp:(CEMarker *)marker;

/** Notifies the delegate when a marker the user touches down on a marker.
 
 @param marker The marker which was touched.
  @return Whether the event has been consumed.
 */
- (BOOL)markerTouchDown:(CEMarker *)marker;

/** Notifies the delegate when a marker the user taps on a marker.
 
 @param marker The marker which was tapped.
  @return Whether the event has been consumed.
 */
- (BOOL)markerTapped:(CEMarker *)marker;

/** Notifies the delegate when a marker the user double taps on a marker.
 
 @param marker The marker which was tapped.
  @return Whether the event has been consumed.
 */
- (BOOL)markerDoubleTapped:(CEMarker *) marker;

/** Notifies the delegate when a marker the user long presses on a marker.
 
 @param marker The marker which was pressed.
  @return Whether the event has been consumed.
 */
- (BOOL)markerLongPressed:(CEMarker *) marker;

/** Notifies the delegate when a marker starts being dragged
 
 @param marker The marker being dragged
 */
- (void)markerDidBeginDragging:(CEMarker *)marker;

/** Notifies the delegate when a marker is dragged.  This is called at each movement
 
 @param marker The marker being dragged
 */
- (void)markerDragged:(CEMarker *)marker;

/** Notifies the delegate when a marker stops being dragged
 
 @param marker The marker being dragged
 */
- (void)markerWillEndDragging:(CEMarker *)marker;

@end

/**
 A class representing a marker on the map.  A marker is any arbitrary annotation on the map.  It is represented by an image, either from disk or from memory.

 */

@interface CEMarker : NSObject

/** 
 * @name Initializers
 *  
 */

/** Basic initialization.  Loads the marker with no image.  If this constructor is used, you must load the image with one of the loader methods or nothing will show for this marker. */
- (id)init;

/** Initialize an image with a bundle resource name. The image will be cached and reused in all markers using this resource.
 *
 * @param resource The name of the bundle resource of the image to display for this marker.
 */
- (id)initWithImageNamed:(NSString *)resource;

/** Initialize with an image from disk. The image will be cached and reused in all markers using this file.
 
 @param file The full filepath of the image to display for this marker.
 */
- (id)initWithFile:(NSString *)file;

/** Initialize with a UIImage.
 *
 * @param image The UIImage object to display for this marker. The image will not be cached. If you are loading an image from a resource or file,
 * we recommend you use the appropriate init method.
 */
- (id)initWithImage:(UIImage *)image;

/** Initialize with a UIImage.
 *
 * @param image The UIImage object to display for this marker. The image will be cached under the specified key if set and not empty.
 * To access this image again easily, you can use initWithKey to reuse the same image.
 */
- (id)initWithImage:(UIImage *)image withKey:(NSString *)key;

/** Initialize with the image given for the specified key.
 * @param key The key to reuse. If this key is undefined, the marker will appear blank until reloaded.
 */
- (id)initWithKey:(NSString *)key;

/** 
 Initialize with a UIView.  This method will take a snaphsot of the view in its current state and render it into a UIImage.
 This is NOT a live view.  Individual UI elements will not respond to touch events in Objective-C.  This is the equivalent of
 initializing this marker with an image that looks exactly like this view.

 @param view The view to take a snapshot of
 */
- (id)initWithView:(UIView *)view;

/** 
 * @name Loaders
 *  
 */

/** Reload the marker with an image with a bundle resource name. The image will be cached and reused in all markers using this resource.
 
 @param resource The name of the bundle resource of the image to display for this marker.
 */
- (void)loadFromImageNamed:(NSString *)resource;

/** Reload the marker with an image from disk. The image will be cached and reused in all markers using this file.
 *
 * @param file The full filepath of the image to display for this marker.
 */
- (void)loadFromFile:(NSString *)file;

/** Reload the marker with a UIImage. The image will not be cached. If you are loading an image from a resource or file,
 * we recommend you use the appropriate init method.
 * @param image The UIImage object to display for this marker.
 */
- (void)loadFromUIImage:(UIImage *)image;

/** Reload the marker with a UIImage. The image will not be cached. If you are loading an image from a resource or file,
 * we recommend you use the appropriate init method.
 * @param image The UIImage object to display for this marker.
 */
- (void)loadFromUIImage:(UIImage *)image withKey:(NSString *)key;

/** Reload the marker with the image specified for the given key. If no image is found, the marker will appear blank until loaded again.
 * @param key The key to reuse on this marker.
 */
- (void)loadFromKey:(NSString *)key;

/**
 Reload the marker with a UIView.  This method will take a snaphsot of the view in its current state and render it into a UIImage.
 This is NOT a live view.  Individual UI elements will not respond to touch events in Objective-C.  This is the equivalent of
 initializing this marker with an image that looks exactly like this view.
 
 @param view The view to take a snapshot of
 */
- (void)loadFromView:(UIView *)view;

/** Add an attachment to the marker.
 @param child The child to add to the marker.
 */
- (void)addMarker:(CEMarker *)child;

/** Add a label to the marker.
 @param label The label to add to the marker.
 */
- (void)addLabel:(CELabel *)label;

/**
 @param label The label to remove from this marker.
 */
- (void)removeLabel:(CELabel *)label;

- (void)removeFromParent;

/** Add an attachment view to the marker.
 
 @param attachment The attachment view to add to the marker.
 @param key key used to identify the attachment.
 */
- (void)addAttachmentView:(CEMarkerAttachmentView *)attachment forKey:(NSString *)key;

/** Returns the attachment view for the given key.
 * @param key The key of the attachment view to retrieve.
 * @return The attachment view or nil of no attachment was found.
 */
- (CEMarkerAttachmentView *)attachmentViewForKey:(NSString *)key;

/** Removes an attachment view from the marker.
 * @param key key used to identify the attachment.
 * @return Whether a view was removed
 */
- (BOOL)removeAttachmentView:(NSString *)key;

/**
 * @name Properties
 *  
 */

/** Display size of the marker. */
@property (nonatomic, assign) CGSize size;

/** Position of the marker. */
@property (nonatomic, assign) CLLocationCoordinate2D position;

/** Anchor point of the marker. This value determines which pixel of the image is anchored (pinned) to the position of the marker.
 * (0,0) represents the top left corner of the marker. (1,1) represents the bottom right corner of the marker.
 */
@property (nonatomic, assign) CGPoint anchorPoint;

/** Attachment anchor point of the marker. This value determines which pixel of the parent marker this marker should be positioned at.
 * (0,0) represents the top left corner of the marker. (1,1) represents the bottom right corner of the marker.
 */
@property (nonatomic, assign) CGPoint attachmentAnchorPoint;

/** Marker delegate */
@property (nonatomic, weak) id <CEMarkerDelegate> delegate;

/** z-index of this marker.  
 
 The z-index determines the order markers are rendered in.  Markers with a higher z-index are rendered last, thus causing them to be drawn over other markers with a lower z-index.
 */
@property (nonatomic, assign) CGFloat zIndex;

/** Whether the marker is draggable */
@property (nonatomic, assign) BOOL draggable;

/** Whether the marker is currently being dragged */
@property (nonatomic, assign) BOOL dragging;

/** Whether the user can interact with this marker. Defaults to YES */
@property (nonatomic, assign) BOOL userInteractionEnabled;

/** The alpha value of the marker. 
 * Set this to a negative value to enable automatic fade in and out based on visibility.
 */
@property (nonatomic, assign) CGFloat alpha;

/** The rotation value of the marker. Degrees */
@property (nonatomic, assign) CGFloat rotation;

/** The rotation value of the marker. 1.0 is 100% */
@property (nonatomic, assign) CGFloat scale;

/** How long it takes the marker to fade in and out when visibility changes. Visibility can be changed through the "hidden" property, or by CEMarkerGroup's collision detection*/
@property (nonatomic, assign) NSTimeInterval fadeTime;

/** Show or hide this marker. This will internally animate the alpha of your marker to either 0 for hidden, or the value of the alpha property if not hidden.
 * Example:
 * marker.alpha = 0.5;
 * marker.hidden = YES; // Will animate marker's alpha from its current value (0.5) to 0, over fadeTime seconds.
 * ...
 * marker.hidden = NO; // Will animate marker's alpha from its current value (0), to it's alpha property (0.5), over fadeTime seconds.
 */
@property (nonatomic, assign) BOOL hidden;

/** The collision priority of this marker. If CEMarkerGroup's collisions are enabled, markers that collide will begin being automatically hidden upon collision.
 * The marker that is left on screen will be the highest collisionPriority marker of the colliding markers.
 */
@property (nonatomic, assign) CGFloat collisionPriority;

/** Whether the marker becomes highlighted on touch down.  The marker will be blended with the highlightColor */
@property (nonatomic, assign) BOOL highlightsOnTouch;

/** The color to blend the marker with when highlighted by the user. */
@property (nonatomic, assign) UIColor *highlightColor;

/** The size of the marker. Will default to the image's size if not set by the user, or if width or height are equal to 0. */
@property (nonatomic, assign) CGSize screenSize;

/** The readonly screen position of the marker. */
@property (nonatomic, assign, readonly) CGPoint screenPosition;

/** The readonly screen bounds of the marker. */
@property (nonatomic, assign, readonly) CGRect screenBounds;

/** A list of attached markers. */
@property (nonatomic, strong, readonly) NSMutableArray *childMarkers;

/** A list of attached labels. */
@property (nonatomic, strong, readonly) NSMutableArray *labels;

/** A tag object associated with this marker */
@property (nonatomic, strong) id tag;

/** The marker's title. This is a string value describing the marker which will be used in the default info window when selected */
@property (nonatomic, strong) NSString *title;

@property (nonatomic, weak) CEMarker *parent;

/** The anchor point of the info window attached to this marker */
@property (nonatomic, assign) CGPoint infoWindowAnchor;

@end
