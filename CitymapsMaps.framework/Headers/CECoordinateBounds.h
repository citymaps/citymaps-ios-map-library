//
//  CECoordinateBounds.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 10/12/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CEMapTypes.h"

@interface CECoordinateBounds : NSObject

@property (assign, nonatomic) CLLocationCoordinate2D southWest;
@property (assign, nonatomic) CLLocationCoordinate2D northEast;

@property (readonly, getter=isValid) BOOL valid;
@property (assign, nonatomic, readonly) CLLocationCoordinate2D center;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coord1
              coordinate:(CLLocationCoordinate2D)coord2;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coord1 andRadius:(double)radius;

- (id)initWithSouth:(double)south west:(double)west north:(double)north east:(double)east;

- (id)initWithRegion:(CEVisibleRegion)region;

- (CECoordinateBounds *)includingCoordinate:(CLLocationCoordinate2D)coordinate;
- (CECoordinateBounds *)includingBounds:(CECoordinateBounds *)other;
- (BOOL)containsCoordinate:(CLLocationCoordinate2D)coordinate;
- (BOOL)containsBounds:(CECoordinateBounds *)other;
- (BOOL)intersectsBounds:(CECoordinateBounds *)other;
- (BOOL)equals:(CECoordinateBounds *)other;

- (void)pad:(double)padPercent;


@end
