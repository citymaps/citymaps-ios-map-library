//
//  CETile.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 2/17/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CEMapTypes.h"

@interface CETile : NSObject <NSCopying>

- (id)init;
- (id)initWithX:(NSUInteger)x Y:(NSUInteger)y andZoom:(NSUInteger)zoom;

- (id)copyWithZone:(NSZone *)zone;

- (NSUInteger) hash;

- (BOOL)isEqual:(id)object;

@property (assign, nonatomic) NSUInteger x;
@property (assign, nonatomic) NSUInteger y;
@property (assign, nonatomic) NSUInteger zoom;
@property (assign, nonatomic, readonly) NSUInteger hash;

@end
