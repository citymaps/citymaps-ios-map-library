//
//  CERouteInstruction.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "CERoutingTypes.h"

typedef NS_ENUM(NSInteger, CERouteInstructionType) {
    CERouteInstructionTypeNone = 0,
    CERouteInstructionTypeStart = 1,
    CERouteInstructionTypeStartRight = 2,
    CERouteInstructionTypeStartLeft = 3,
    CERouteInstructionTypeDestination = 4,
    CERouteInstructionTypeDestinationRight = 5,
    CERouteInstructionTypeDestinationLeft = 6,
    CERouteInstructionTypeBecomes = 7,
    CERouteInstructionTypeContinue = 8,
    CERouteInstructionTypeSlightRight = 9,
    CERouteInstructionTypeRight = 10,
    CERouteInstructionTypeSharpRight = 11,
    CERouteInstructionTypeUturnRight = 12,
    CERouteInstructionTypeUturnLeft = 13,
    CERouteInstructionTypeSharpLeft = 14,
    CERouteInstructionTypeLeft = 15,
    CERouteInstructionTypeSlightLeft = 16,
    CERouteInstructionTypeRampStraight = 17,
    CERouteInstructionTypeRampRight = 18,
    CERouteInstructionTypeRampLeft = 19,
    CERouteInstructionTypeExitRight = 20,
    CERouteInstructionTypeExitLeft = 21,
    CERouteInstructionTypeStayStraight = 22,
    CERouteInstructionTypeStayRight = 23,
    CERouteInstructionTypeStayLeft = 24,
    CERouteInstructionTypeMerge = 25,
    CERouteInstructionTypeRoundaboutEnter = 26,
    CERouteInstructionTypeRoundaboutExit = 27,
    CERouteInstructionTypeFerryEnter = 28,
    CERouteInstructionTypeFerryExit = 29,
    CERouteInstructionTypeTransit = 30,
    CERouteInstructionTypeTransitTransfer = 31,
    CERouteInstructionTypeTransitRemainOn = 32,
    CERouteInstructionTypeTransitConnectionStart = 33,
    CERouteInstructionTypeTransitConnectionTransfer = 34,
    CERouteInstructionTypeTransitConnectionDestination = 35,
    CERouteInstructionTypePostTransitConnectionDestination = 36
};

@class CERoute;

@interface CERouteTransitInfo : NSObject

@property (strong, nonatomic, nonnull) UIColor *color;
@property (strong, nonatomic, nonnull) UIColor *textColor;
@property (strong, nonatomic, nonnull) NSString *shortName;
@property (strong, nonatomic, nonnull) NSString *longName;

+ (nonnull UIColor *)colorFromInt:(NSInteger)color;

@end

@interface CERouteInstruction : NSObject

@property (nonatomic, assign) CERouteInstructionType type;
@property (nonatomic, assign) NSInteger beginShapeIndex;
@property (nonatomic, assign) NSInteger endShapeIndex;
@property (nonatomic, assign) CGFloat length;
@property (nonatomic, assign) NSInteger time;
@property (nonatomic, strong, nonnull) NSString *instruction;
@property (nonatomic, strong, nonnull) NSString *verbalPreTransitionInstruction;
@property (nonatomic, strong, nonnull) NSString *verbalTransitionAlertInstruction;
@property (nonatomic, strong, nonnull) NSString *verbalPostTransitionInstruction;
@property (nonatomic, strong, nonnull) NSArray<NSString *> *streetNames;
@property (nonatomic, strong, nullable) CERouteTransitInfo *transitInfo;
@property (nonatomic, assign) CERoutingTravelMode travelMode;
@property (nonatomic, assign) CERoutingTravelType travelType;

@property (nonatomic, assign, nonnull) CERoute *route;
@property (nonatomic, assign) NSInteger index;

@end
