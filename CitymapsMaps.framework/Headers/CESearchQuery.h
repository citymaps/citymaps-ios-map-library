//
//  CESearchQuery.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 12/17/15.
//  Copyright © 2015 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEMapTypes.h"
#import "CESearchResult.h"

@interface CESearchQuery : NSObject

@property (assign, nonatomic) CESearchType type;
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) CLLocationCoordinate2D location;
@property (assign, nonatomic) double radius;
@property (assign, nonatomic) NSUInteger minRating;
@property (strong, nonatomic) NSString *pricesString;
@property (assign, nonatomic) NSUInteger maxVenueResults;
@property (assign, nonatomic) NSUInteger maxRegionResults;
@property (assign, nonatomic) NSUInteger maxTaxonomyResults;
@property (assign, nonatomic) NSUInteger maxAddressResults;
@property (strong, nonatomic) NSString *userId;
@property (assign, nonatomic) BOOL myPlaces;
@property (assign, nonatomic) BOOL friendsPlaces;

@end
