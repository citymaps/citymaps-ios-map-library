//
//  CERoute.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "CERoutingTypes.h"
#import "CECoordinateBounds.h"
#import "CERoutingRequest.h"

@class CERouteInstruction;

@interface CERoute : NSObject

@property (nonatomic, readonly, assign) CLLocationCoordinate2D start;
@property (nonatomic, readonly, assign) CLLocationCoordinate2D end;
@property (nonatomic, readonly, assign) CLLocationCoordinate2D *points;
@property (nonatomic, readonly, assign) NSInteger numPoints;
@property (nonatomic, readonly, strong) NSArray<CERouteInstruction *> *instructions;
@property (nonatomic, readonly, assign) CGFloat distance;
@property (nonatomic, readonly, assign) NSInteger time;
@property (nonatomic, readonly, assign) NSUInteger mode;
@property (nonatomic, readonly, strong) CECoordinateBounds *boundingBox;
@property (nonatomic, readonly, assign) CERoutingUnitSystem units;

@end
