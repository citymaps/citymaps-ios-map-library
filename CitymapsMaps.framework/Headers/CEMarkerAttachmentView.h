//
//  CEMarkerAttachmentView.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 11/27/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CEMarker.h"

/** 
 This class allows you to attach a UIView to the map, giving you the power of the iOS UIKit SDK, while still allowing 
 this element to be part of the map.  The view must be attached to a marker. 
 */

@interface CEMarkerAttachmentView : UIView

/** The anchor point of the attachment relative to the marker.  Expressed as UV coordinates between 0.0 and 1.0 */
@property (assign, nonatomic) CGPoint markerAnchor;

/** The key associated with this attachment view.  This unique key will be used to identify this attachment */
@property (strong, nonatomic) NSString *key;

/** The marker this attachment is associated with */
@property (strong, nonatomic) CEMarker *marker;

@end
