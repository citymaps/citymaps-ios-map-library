//
//  CERegion.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/20/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEMapTypes.h"

/** Regions allow you to define geofencing areas on the application.  
 
 The idea of geofencing is that when a user enters or exits an area, the application will react to this in a certain way.  Regions allow this by giving the application a means to track when a user enters or exits a circular area.  Regions are defined by a longitude/latitude location and a radius.  Different shapes may be available in the future.
 */

@interface CERegion : NSObject

/** 
 * @name Initializers
 *  
 */

/** Initialize a new region with a center and radius.
 
 @param center The center of the region.
 @param radius The radius of the region in meters.
 */
- (id)initWithCenter:(CLLocationCoordinate2D)center radius:(double)radius;

/** 
 * @name Utility
 *  
 */

/** Check whether the region contains a location.
 
 @param point The location to check against the region.
 @return Whether the point is contained in the region.
 */
- (BOOL)containsPoint:(CLLocationCoordinate2D)point;

/** 
 * @name Properties
 *  
 */

/** Whether the region is active. */
@property (assign, nonatomic) BOOL active;

/** Center point of the region. */
@property (assign, nonatomic) CLLocationCoordinate2D center;

/** Radius of the region in meters. */
@property (assign, nonatomic) double radius;

/** User-defined identifier for this region. */
@property (strong, nonatomic) id identifier;

/** User-defined description for this region. */
@property (strong, nonatomic) NSString *regionDescription;

@end

