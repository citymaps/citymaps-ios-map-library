//
//  CERoutingTypes.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/5/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@class CERoute;

typedef void (^CERoutingCompletionBlock)(NSArray<CERoute*> *routes, NSError *error);
typedef void (^CETimeDistanceCompletionBlock)(NSInteger time, CGFloat distance, NSError *error);

typedef NS_ENUM(NSInteger, CERoutingUnitSystem) {
    CERoutingUnitSystemImperial,
    CERoutingUnitSystemMetric
};

typedef NS_ENUM(NSInteger, CERoutingMode) {
    CERoutingModeDriving,
    CERoutingModePublicTransport,
    CERoutingModeWalking,
    CERoutingModeBicycle
};

typedef NS_ENUM(NSInteger, CERoutingTravelMode)  {
    CERoutingTravelModeDriving = 0,
    CERoutingTravelModeWalking = 1,
    CERoutingTravelModeTransit = 2
};

typedef NS_ENUM(NSInteger, CERoutingTravelType)  {
    CERoutingTravelTypeRoad = 0,
    CERoutingTravelTypeFoot = 1,
    CERoutingTravelTypeMetro = 2
};
