/** 
 This class allows a user to create a new map view which is already predefined to work with Citymaps.  No setup is required by the user. 
 
 */

#import "CEMapView.h"
#import "CECitymapsTypes.h"
#import "CEBusinessLayer.h"
#import "CEVectorLayer.h"
#import "CEBusinessData.h"
#import "CERoutingRequest.h"


static NSString *kCEBundleAPIKey = @"CitymapsMapAPIKey";

@interface CECitymapsMapViewOptions : CEMapViewOptions

- (id)initWithAPIKey:(NSString *)apiKey;

@property (nonatomic, strong) NSString *apiKey;
@property (nonatomic, strong) NSString *style;
@property (nonatomic, assign) BOOL mapLayerEnabled;
@property (nonatomic, assign) BOOL buildingsLayerEnabled;
@property (nonatomic, assign) BOOL businessLayerEnabled;
@property (nonatomic, assign) BOOL beginUpdatingLocationHeading;

@end
/** A convenience class used to quickly initialize a CEMapView with Citymaps functionality.
 */
@interface CECitymapsMapView : CEMapView

/**
 * @name Initialization
 *  
 */

/** Create a new Citymaps map view.

@param frame Frame of the map view
@param apiKey Citymaps API Key
*/
- (id)initWithFrame:(CGRect)frame andAPIKey:(NSString *)apiKey;

/** Create a new Citymaps map view.
 
 @param frame Frame of the map view
 @param options An immutable dictionary of options for the map
 */
- (id)initWithFrame:(CGRect)frame options:(CECitymapsMapViewOptions *)options;

/**
 * @name Style
 */

/** Updates the map style from a file in the filesystem.
 * @param mapConfig The file to load from.
 */
- (void)updateMapStyleFromFile:(NSString*)mapConfig;

/** Updates the map style from an application resource.
 * @param mapConfig The resources to load from.
 */
- (void)updateMapStyleFromResource:(NSString*)mapConfig;

/**
 * @name Business Management
 *  
 */

/** Apply a business filter to the map
 
 @param filter The filter to apply
 */
- (void)applyBusinessFilter:(CEBusinessFilter *)filter;

/** Remove any active business filter */
- (void)removeBusinessFilter;

/** Add an additional business to an active filter
 
 The business state may be one of the following values:
 
 - kCEBusinessStateNormal - The default state of businesses.  Set this to remove a business state.
 - kCEBusinessState2X - Doubles the size of the business.
 - kCEBusinessStateSelected - Puts a blue halo around the business.
 - kCEBusinessStateHidden - Hides the business from view.
 
 @param data - The business data for this business
 @param state - The state for this business
 */
- (void)addBusinessToActiveFilter:(CEBusinessData *)data state:(CEBusinessMarkerState)state;

/** Remove a business from an active filter
 *
 *@param bid - The business ID to remove
 */
- (void)removeBusinessFromActiveFilter:(NSString *)bid;

/** Set the state of a particular business.
 *
 *The business state may be one of the following values:
 *
 * - kCEBusinessStateNormal - The default state of businesses.  Set this to remove a business state.
 * - kCEBusinessState2X - Doubles the size of the business.
 * - kCEBusinessStateSelected - Puts a blue halo around the business.
 * - kCEBusinessStateHidden - Hides the business from view.
 *
 * @param bid - The business ID
 * @param state - The new state for this business
 */
- (void)setBusinessState:(NSString *)bid state:(CEBusinessMarkerState)state;

/** Reset the state of all businesses to kCEBusinessStateNormal */
- (void)resetBusinessStates;

/** Updates the map style from a file on the filesystem.
 * @param configFile The file to update the style from.
 */
- (void)updateStyleFromFile:(NSString*)configFile;

/** Updates the map style from an application resource.
 * @param configFile The resource to update the style from.
 */
- (void)updateStyleFromResource:(NSString*)configFile;

/** Updates the map style from an xml string.
 * @param configFile The xml string to update the style from.
 */
- (void)updateStyleFromString:(NSString *)data;

/**
 * @name Properties
 *  
 */

/** Base map layer */
@property (nonatomic, strong, readonly) CEVectorLayer *baseLayer;

/** Business layer */
@property (nonatomic, strong, readonly) CEBusinessLayer *businessLayer;

/** Building layer */
@property (nonatomic, strong, readonly) CEVectorLayer *buildingsLayer;

/** Business delegate */
@property (nonatomic, strong) id<CEBusinessDelegate> businessDelegate;

/** Current active business filter */
@property (nonatomic, strong, readonly) CEBusinessFilter *filter;

/** The URL currently being used to retrieve map tiles. */
@property (strong, nonatomic) NSString *mapTileURL;

/** The URL currently being used to retrieve business tiles. */
@property (strong, nonatomic) NSString *businessTileURL;

/** Forces the map to use only offline tiles */
@property (assign, nonatomic) BOOL forceOffline;

@end
