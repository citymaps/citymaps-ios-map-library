//
//  CEMapTypes.h
//  vectormap2
//
//  Created by Lion User on 10/03/2013.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#ifdef __cplusplus
extern "C"
{
#endif
    
    /**
     * This class defines a longitude, latitude pair.
     */
    
    typedef enum {
        kCEMapProjectionMercator,
        kCEMapProjectionLonLat
    } CEMapProjection;
    
    typedef struct {
        CLLocationCoordinate2D center;
        float zoom;
        double orientation;
        double tilt;
    } CEMapPosition;
    
    static inline CEMapPosition CEMapPositionMake(CLLocationCoordinate2D center, float zoom, double orientation, double tilt) {
        CEMapPosition pos;
        pos.center = center;
        pos.zoom = zoom;
        pos.orientation = orientation;
        pos.tilt = tilt;
        return pos;
    }
    
    typedef struct {
        CLLocationCoordinate2D nearLeft;
        CLLocationCoordinate2D nearRight;
        CLLocationCoordinate2D farLeft;
        CLLocationCoordinate2D farRight;
    } CEVisibleRegion;
    
    static inline CEVisibleRegion CEVisibleRegionMake(CLLocationCoordinate2D nearLeft, CLLocationCoordinate2D nearRight, CLLocationCoordinate2D farLeft, CLLocationCoordinate2D farRight) {
        CEVisibleRegion region;
        region.nearLeft = nearLeft;
        region.nearRight = nearRight;
        region.farLeft = farLeft;
        region.farRight = farRight;
        return region;
    }
    
    static inline bool CELocationCoordinateEqual(CLLocationCoordinate2D p1, CLLocationCoordinate2D p2) {
        return p1.longitude == p2.longitude && p1.latitude == p2.latitude;
    }
    
#ifdef __cplusplus
}
#endif

#import "CECoordinateBounds.h"
