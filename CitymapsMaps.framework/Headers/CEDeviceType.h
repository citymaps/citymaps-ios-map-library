//
//  CEDeviceType.h
//  vectormap
//
//  Created by Adam Eskreis on 3/22/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kCEDeviceUnknown,
    kCEDeviceiPhone3GS,
    kCEDeviceiPhone4,
    kCEDeviceiPhone4S,
    kCEDeviceiPhone5,
    kCEDeviceiPhone5s,
    kCEDeviceiPhone6,
    kCEDeviceiPhone6Plus,
    kCEDeviceiPad,
    kCEDeviceiPad2,
    kCEDeviceiPad3,
    kCEDeviceiPad4,
    kCEDeviceiPadMini,
    kCEDeviceSimulator,
    kCEDeviceSimulatorRetina,
} CEDeviceName;

@interface CEDeviceType : NSObject

+ (CEDeviceName)currentDeviceName;
+ (BOOL)isRetina;
+ (BOOL)isPhone;
+ (BOOL)isTablet;
+ (BOOL)isPod;
+ (BOOL)isSimulator;
+ (BOOL)isLowEnd;
+ (NSString*)version;
+ (BOOL)isVersionOrNewer:(NSString*)version;
+ (BOOL)isVersion:(NSString*)version;
+ (BOOL)isNewerThanVersion:(NSString*)version;
+ (BOOL)isOlderThanVersion:(NSString*)version;

@end
