//
//  CEAnimationScript.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 8/22/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEMapTypes.h"
#import "CEMapView.h"

/** A class to be used to define a sequence of animated movements. */
@interface CEAnimationScript : NSObject

/**
 * @name Animation Steps
 */

/**
 * Adds a complete MapPosition animation to the script.
 * @param position The map position to animate to.
 */
- (void)moveToPosition:(CEMapPosition)position;
    
/**
* Adds a complete MapPosition animation to the script.
* @param position The map position to animate to.
* @param delay The amount of time (in seconds) the script should wait before starting this step.
*/
- (void)moveToPosition:(CEMapPosition)position delay:(float)delay;
    
/**
 * Add an animated pan movement to the script.
 * @param point The CLLocationCoordinate2D to move to.
 */
- (void)moveTo:(CLLocationCoordinate2D)point;

/**
 * Add an animated pan movement to the script.
 * @param point The CLLocationCoordinate2D to move to.
 * @param delay The amount of time (in seconds) the script should wait before starting this step.
 */
- (void)moveTo:(CLLocationCoordinate2D)point delay:(float)delay;

/**
 * Add an animated pan and zoom movement to the script.
 * @param point The CLLocationCoordinate2D to move to.
 * @param zoom The zoom level to zoom to.
 */
- (void)moveTo:(CLLocationCoordinate2D)point andZoom:(int)zoom;

/**
 * Add an animated pan and zoom movement to the script.
 * @param point The CLLocationCoordinate2D to move to.
 * @param zoom The zoom level to zoom to.
 * @param delay The amount of time (in seconds) the script should wait before starting this step.
 */
- (void)moveTo:(CLLocationCoordinate2D)point andZoom:(int)zoom delay:(float)delay;

/**
 * Add an animated zoom movement to the script.
 * @param zoom The zoom level to zoom to.
 */
- (void)zoomTo:(int)zoom;

/**
 * Add an animated zoom movement to the script.
 * @param zoom The zoom level to zoom to.
 * @param delay The amount of time (in seconds) the script should wait before starting this step.
 */
- (void)zoomTo:(int)zoom delay:(float)delay;

/**
 * Add an animated rotation to the script.
 * @param orientation The orientation to rotate to.
 */
- (void)rotateTo:(double)orientation;

/**
 * Add an animated rotation to the script.
 * @param orientation The orientation to rotate to.
 * @param delay The amount of time (in seconds) the script should wait before starting this step.
 */
- (void)rotateTo:(double)orientation delay:(float)delay;

/**
 * @name Play
 */

/**
 * Play the animation script.
 * @param map The map to play the animation on.
 */
- (void)play:(CEMapView *)map;

@end
