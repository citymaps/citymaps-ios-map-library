//
//  CEOfflineContent.h
//  MapEngineLibraryIOS
//
//  Created by Edward Kimmel on 6/6/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CEOfflineContent : NSObject

+ (void)init;

+ (void)syncOfflineContentForUser:(NSString *)userId andToken:(NSString *)token;
+ (void)syncOfflineContentForUser:(NSString *)userId andToken:(NSString *)token forOfflinePackage:(NSString *)packageId;

+ (NSString *)offlineHostname;
@end
