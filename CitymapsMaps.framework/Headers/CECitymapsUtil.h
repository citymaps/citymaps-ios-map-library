//
//  CECitymapsUtil.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/31/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BusinessData.h"

@class CEBusinessData;
namespace citymaps
{
    class CECitymapsUtil
    {
    public:
        
        static void CreateBusinessData(CEBusinessData* ceData, BusinessData& outData);        
        static CEBusinessData* CreateBusinessData(const BusinessData &data);
        static NSArray* CreateBusinessDataArray(const std::vector<BusinessData> &data);
        static NSString* NSStringFromUTF16String(const std::u16string& string);
    };
}
