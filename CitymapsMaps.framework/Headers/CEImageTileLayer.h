#import "CETileLayer.h"

/** This class allows the creation of an image-based tile layer.  Image based tile layers are the most common type of map layer found.  
 
 In order to create an image tile layer, you must provide a CEImageTileSource, with valid URLs.  CEImageTileSource requires a mercator-based tile server, similar to OpenStreetMap.org.
 
 See CETileLayer for a list of available options.
 
 */

@interface CEImageTileLayer : CETileLayer

@end
