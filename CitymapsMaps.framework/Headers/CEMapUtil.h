//
//  CEMapUtil.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 6/17/15.
//  Copyright (c) 2015 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEMapTypes.h"

@class CETile;
@interface CEMapUtil : NSObject

+ (double)metersPerMile;
+ (double)resolutionForZoom:(float)zoom;
+ (float)zoomForResolution:(double)resolution;
+ (double)haversineDistanceBetweenPoints:(CLLocationCoordinate2D)p1 and:(CLLocationCoordinate2D)p2;
+ (CETile*)tileForLonLat:(CLLocationCoordinate2D)lonLat atZoom:(NSInteger)zoom onLayerWithTileSize:(NSInteger)tileSize;

@end
