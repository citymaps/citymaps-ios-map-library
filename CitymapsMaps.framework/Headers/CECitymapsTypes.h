//
//  CECitymapsTypes.h
//  vectormap2
//
//  Created by Adam Eskreis on 8/6/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

typedef NS_ENUM(NSInteger, CEBusinessMarkerState) {
    CEBusinessMarkerStateNormal,
    CEBusinessMarkerStateSelected,
};
