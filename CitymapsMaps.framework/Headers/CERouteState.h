//
//  CERouteState.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 9/8/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface CERouteState : NSObject

@property (nonatomic, assign) NSInteger instruction;
@property (nonatomic, assign) CGFloat distanceToNextInstruction;
@property (nonatomic, assign) CGFloat distanceToDestination;
@property (nonatomic, assign) CGFloat timeToNextInstruction;
@property (nonatomic, assign) CGFloat timeUntilDestination;
@property (nonatomic, assign) CGFloat routeHeading;
@property (nonatomic, assign) CGFloat userVelocity;

@end
