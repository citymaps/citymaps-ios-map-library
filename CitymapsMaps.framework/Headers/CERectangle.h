#import <Foundation/Foundation.h>

#import "CEMapTypes.h"
#import "CEFeature.h"

/**
 * This class represents a user defined rectangle to be placed on a CECanvasLayer.
 */
@interface CERectangle : CEFeature

/**
 * @name Initializers
 */

/**
 * Initializes a new rectangle shape.
 * @param position The center of the rectangle.
 * @param size The half extents of the rectangle.
 */
-(id)initWithPosition:(CLLocationCoordinate2D)position andSize:(CGSize)size;

/**
 * @name Properties
 *  
 */

/** The center of the rectangle. */
@property (nonatomic, assign) CLLocationCoordinate2D position;

/** The half extents of the rectangle. */
@property (nonatomic, assign) CGSize size;

@end
