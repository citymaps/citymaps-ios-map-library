/** This abstract base class is the base class for all map layers.  
 
 Layers are the foundation of all maps.  A map layer is a collection of data which is overlayed on the screen, and controlled by the map.  A map layer can range from anything as simple as an image overlay, to a complex data source.  Several types are layers are built into the Citymaps library, however new layer types can be built with any functionality imaginable.
 
 CELayer accepts the following options:
 
 - minZoom: The lower bound (most zoomed out) the map can zoom to.
 - maxZoom: The upper bound (most zoomed in) the map can zoom to.
 
 */

#import <Foundation/Foundation.h>

@class CEMapView;

/**
 This is the base options class used to initialize CELayers.
 */

@interface CELayerOptions : NSObject

@property (nonatomic, assign) int minZoom;
@property (nonatomic, assign) int maxZoom;
@property (nonatomic, assign) BOOL visible;

@end

/**
 This is the base class representing a layer on a CEMapView.
 */
@interface CELayer : NSObject

/** 
 * @name Initializers
 *  
 */

/** Initialize a new layer with an immutable dictionary of options.
 
 @param options - The options for this layer. Accepted options are:
 
 - minZoom: The lower bound (most zoomed out) the map can zoom to.
 - maxZoom: The upper bound (most zoomed in) the map can zoom to.
 */
- (id)initWithOptions:(CELayerOptions *)options;

/** 
 * @name Properties
 *  
 */

/** The most zoomed out level for this layer. */
@property (assign, nonatomic) int minZoom;

/** The most zoomed in level for this layer. */
@property (assign, nonatomic) int maxZoom;

/** Whether this layer is visible */
@property (assign, nonatomic) BOOL visible;

/** The map this layer is currently attached to.  A layer can only be attached to one map at a time */
@property (nonatomic, weak) CEMapView *map;

@end
