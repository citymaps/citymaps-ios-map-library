#import <Foundation/Foundation.h>

#import "CEBusinessData.h"
#import "CECitymapsTypes.h"

/** An object used to describe a filter which can be applied to the Citymaps business layer.  
 
 This allows the application to show only the specified businesses on the map.  Only one filter can be active at a time. 
 */

@interface CEBusinessFilter : NSObject <NSCopying>

/**
 * @name Initializers
 */

/** 
 * @name Business Methods
 *  
 */

/** Add a business to the filter
 
 @param data A CEBusinessData object representing the business
 @param state The state of the business.   The business state may be one of the following values:
 
 - kCEBusinessStateNormal - The default state of businesses.  Set this to remove a business state.
 - kCEBusinessState2X - Doubles the size of the business.
 - kCEBusinessStateSelected - Puts a blue halo around the business.
 - kCEBusinessStateHidden - Hides the business from view.
 */
- (void)addBusiness:(CEBusinessData *)data state:(CEBusinessMarkerState)state;

/** Add a business to the filter
 
 @param bid The Citymaps business ID.
 @param state The state of the business.   The business state may be one of the following values:
 
 - kCEBusinessStateNormal - The default state of businesses.  Set this to remove a business state.
 - kCEBusinessState2X - Doubles the size of the business.
 - kCEBusinessStateSelected - Puts a blue halo around the business.
 - kCEBusinessStateHidden - Hides the business from view.
 
 @param point The location of the business
 */
- (void)addBusiness:(NSString *)bid state:(CEBusinessMarkerState)state point:(CLLocationCoordinate2D)point;

/** Add a business to the filter
 
 @param bid The Citymaps business ID.
 @param state The state of the business.   The business state may be one of the following values:
 
 - kCEBusinessStateNormal - The default state of businesses.  Set this to remove a business state.
 - kCEBusinessState2X - Doubles the size of the business.
 - kCEBusinessStateSelected - Puts a blue halo around the business.
 - kCEBusinessStateHidden - Hides the business from view.
 
 @param point The location of the business.
 @param logoImageId The Citymaps logo image identifier for this business.
 */
- (void)addBusiness:(NSString *)bid state:(CEBusinessMarkerState)state point:(CLLocationCoordinate2D)point logoImageId:(int)logoImageId;

/** Remove a business from this filter.
 
 @param bid The Citymaps business ID.
 */
- (void)removeBusiness:(NSString *)bid;

/** Determine the equality of two filters, based on the businesses in them
 
 @param other The comparison filter
 */
- (BOOL)equals:(CEBusinessFilter *)other;

/** 
 * @name Properties Methods
 *  
 */

/** The businesses in this filter, represented as CEBusinessData objects. */
@property (strong, nonatomic, readonly) NSMutableArray *businesses;

@end
