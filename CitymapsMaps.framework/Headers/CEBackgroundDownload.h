//
//  CEBackgroundDownload.h
//  MapEngineLibraryIOS
//
//  Created by Edward Kimmel on 1/9/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CEBackgroundDownload;
@protocol CEBackgroundDownloadDelegate <NSObject>

- (void)backgroundDownloadDidStart:(CEBackgroundDownload *)task;
- (void)backgroundDownload:(CEBackgroundDownload *)task didWriteData:(int64_t)bytesWritten
    totalBytesWritten:(int64_t)totalBytesWritten
    totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite;
- (void)backgroundDownload:(CEBackgroundDownload *)task didFinishDownloadingToURL:(NSURL *)location;
- (void)backgroundDownloadDidFail:(CEBackgroundDownload *)task withError:(NSError *)error;

@end

@interface CEBackgroundDownload : NSObject <CEBackgroundDownloadDelegate>


@property (nonatomic, assign, readonly) BOOL cancelled;
@property (nonatomic, assign, readonly) BOOL paused;
@property (nonatomic, assign, readonly) BOOL canResume;

- (void)cancel;
- (void)pause:(void(^)(BOOL canResume)) callback;

@end

@interface CEBackgroundDownloadManager: NSObject <NSURLSessionDownloadDelegate>

+ (CEBackgroundDownloadManager *)sharedInstance;
- (CEBackgroundDownload *)download:(NSURL *)url withDelegate:(id<CEBackgroundDownloadDelegate>) delegate;

/** Cannot be called until the pause block has been called and the canResume value is true. */
- (void)resumeDownload:(CEBackgroundDownload *)resumableDownload;

@end
