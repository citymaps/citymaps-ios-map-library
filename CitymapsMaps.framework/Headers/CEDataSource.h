#import <Foundation/Foundation.h>

@interface CEDataSourceOptions : NSObject

@end

/** An abstract base class for all data sources.  
 
 A data source provides a way for tile sources to retreive their data.  They serve as an encapsulation of the data retreival process, and can be served from multiple sources (i.e. network, disk).  A data source is required for CETileSource to properly retreive data.  
 
 See CETileSource for more information about tile and data sources in general.
 */
@interface CEDataSource : NSObject

/** 
 * @name Initializers
 *  
 */

/** Initialize a new data source with a set of options
 
 @param options A dictionary of options used to initialize this data source.
 */

- (id)initWithOptions:(CEDataSourceOptions *)options;

@end
