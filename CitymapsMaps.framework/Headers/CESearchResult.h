//
//  CESearchResult.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 12/17/15.
//  Copyright © 2015 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CEMapTypes.h"

typedef NS_ENUM(NSInteger, CESearchResultType)
{
    kCESearchResultVenue,
    kCESearchResultRegion,
    kCESearchResultCategory,
    kCESearchResultTag,
    kCESearchResultChain,
    kCESearchResultAddress
};

typedef NS_ENUM(NSInteger, CESearchType)
{
    CESearchTypeNone,
    CESearchTypeQuery,
    CESearchTypeFilter,
    CESearchTypeChain,
    CESearchTypeMyPlaces,
    CESearchTypeFriendsPlaces,
    CESearchTypeDeals,
    CESearchTypeBusiness,
    CESearchTypeTag,
    CESearchTypeCategory,
    CESearchTypeRegion,
    CESearchTypeAutocomplete,
    CESearchTypeCustom
};

@interface CESearchResult : NSObject

@property (assign, nonatomic) float score;
@property (assign, nonatomic) CESearchResultType type;
@property (strong, nonatomic) NSString *objectId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *zip;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *phrases;
@property (assign, nonatomic) CLLocationCoordinate2D location;
@property (assign, nonatomic) NSInteger visibility;
@property (assign, nonatomic) NSInteger logoImage;
@property (assign, nonatomic) NSInteger categoryIcon;
@property (assign, nonatomic) NSInteger categoryId;
@property (strong, nonatomic) NSString *category;
@property (assign, nonatomic) NSInteger price;
@property (assign, nonatomic) float rating;

@end
