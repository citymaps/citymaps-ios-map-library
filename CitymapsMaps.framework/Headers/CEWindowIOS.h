//
//  CEGLView.h
//  vectormap
//
//  Created by Adam Eskreis on 11/2/12.
//  Copyright (c) 2012 Adam Eskreis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import "CELocationParams.h"
#import "CELocationProvider.h"

/**
 This is the base class for CEMapView. It manages the rendering context as well as operating system services.
 */
@interface CEWindowIOS : UIView
/** 
 * @name Initializers
 *  
 */

/**
 * Initialize a new window
 
 @param frame - Frame of the map view.
 @return A new window instance.
*/
- (id)initWithFrame:(CGRect)frame;

- (void)locationAuthorizationStatusDidChange:(CLAuthorizationStatus)status;

/** Add a target/selector to the run loop.  This will be called before each frame is updated and rendered. 
 
 @param target The target to add.
 @param selector The selector to be called.
 @param object An optional object to be sent to this selector.
 */
- (void)addRunLoopTarget:(id)target selector:(SEL)selector object:(id)object;

/** Remove a target/selector to the run loop.  This selector will no longer be called.
 
 @param target The target to be removed.
 @param selector The selector to be removed.
 */
- (void)removeRunLoopTarget:(id)target selector:(SEL)selector;

/** Used when the app receives a memory warning. */
- (void)didReceiveMemoryWarning:(NSNotification *)notification;

/** 
 * @name Sensors
 *  
 */

/**
 * Begin tracking user's GPS location. It will use the default CELocationParams.
 */
- (void)startUpdatingLocation;

/**
 * Begin tracking user's orientation.
 */
- (void)startUpdatingHeading;

/**
 * Begin tracking user's GPS location.
 * @param params - The parameters to use when enabling GPS service.
 */
- (void)startUpdatingLocationWithParams:(CELocationParams*)params;

/**
 * Stop tracking user's GPS location.
 */
- (void)stopUpdatingLocation;

/**
 * Stop tracking user's orientation.
 */
- (void)stopUpdatingHeading;

/** 
 * @name Application Parameters
 */

/**
 * Takes a snapshot of the view
 */
- (UIImage*)snapshot;

/**
 * Set the window's background color.
 * @param color A UIColor of type kCGColorSpaceModelMonochrome or kCGColorSpaceModelRGB. Other types are not supported.
 */
- (void)setBackgroundColor:(UIColor*)color;

/** Called after the application state is updated.  This is called automatically.  Do not call this directly. */
- (void)postUpdate;

/**
 * @name Properties
 *
 */

/** Whether network caching is allowed. */
@property (assign, nonatomic) BOOL networkCacheEnabled;

@property (strong, nonatomic) CELocationProvider *locationProvider;

@end
