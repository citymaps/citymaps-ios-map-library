//
//  CECitymapsUtil.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 10/31/13.
//  Copyright (c) 2013 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <CitymapsEngine/Citymaps/Layer/Util/BusinessData.h>
#include <CitymapsEngine/Offline/Packages/CitymapsOfflinePackage.h>
#include <CitymapsEngine/Offline/Search/SearchResult.h>

@class CEBusinessData;
@class CEOfflinePackage;
@class CESearchQuery;
@class CEOfflineRegion;

namespace citymaps
{
    class CEOfflineUtil
    {
    public:
        
        static void CreateSearchQuery(CESearchQuery* query, SearchQuery& outQuery);
        static NSArray* CreateSearchResultsArray(const std::vector<SearchResult>& data);
        
        static citymaps::CitymapsOfflinePackage CreateCitymapsOfflinePackage(CEOfflinePackage * package);
        static CEOfflinePackage *CreateCitymapsOfflinePackage(const citymaps::CitymapsOfflinePackage& package);
        
        static citymaps::CitymapsOfflineRegion CreateCitymapsOfflineRegion(CEOfflineRegion * region);
        static CEOfflineRegion *CreateCitymapsOfflineRegion(const citymaps::CitymapsOfflineRegion& region);
    };
}
