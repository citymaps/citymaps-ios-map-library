#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CEMapTypes.h"
#import "CEMarker.h"

typedef enum {
    kCELabelHorizontalAlignmentLeft,
    kCELabelHorizontalAlignmentCenter,
    kCELabelHorizontalAlignmentRight
} CELabelHorizontalAlignment;

typedef enum {
    kCELabelVerticalAlignmentTop,
    kCELabelVerticalAlignmentMiddle,
    kCELabelVerticalAlignmentBottom
} CELabelVerticalAlignment;

typedef enum {
    CEFontWeightNormal = 400,
    CEFontWeightBold = 700
} CEFontWeight;

/**
 * A class representing a label that can be attached to a marker.
 */
@interface CELabel : NSObject

/**
 * @name Initializers
 *
 */

- (id)init;

/**
 * @name Attaching
 */

- (void)removeFromParent;

/**
 * @name Properties
 *
 */

/** Text to display. */
@property (nonatomic, strong) NSString *text;

@property (nonatomic, assign) NSUInteger fontSize;
@property (nonatomic, assign) NSUInteger outlineSize;
@property (nonatomic, assign) UIColor *textColor;
@property (nonatomic, assign) UIColor *outlineColor;
@property (nonatomic, assign) CELabelHorizontalAlignment horizontalAlignment;
@property (nonatomic, assign) CELabelVerticalAlignment verticalAlignment;
@property (nonatomic, assign) CEFontWeight fontWeight;

/** Maximum display size of the marker. */
@property (nonatomic, assign) CGSize size;

/** Anchor point of the label.
 * This anchor point determines where the label will attach to its marker, relative to the marker's size. Anchor point values is typically between 0 and 1.
 */
@property (nonatomic, assign) CGPoint anchorPoint;

/** The alpha value of the label.
 * Set this to a negative value to enable automatic fade in and out based on visibility.
 */
@property (nonatomic, assign) float alpha;

@property (nonatomic, weak) CEMarker *parent;

/** A tag object associated with this label */
@property (nonatomic, strong) id tag;


@end
