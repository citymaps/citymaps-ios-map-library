//
//  CEOfflinePackageManager.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 12/16/15.
//  Copyright © 2015 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CEOfflineDownload.h"

typedef void(^OfflinePackageCallback)(NSArray<CEOfflinePackage *> * _Nonnull packages);
typedef void(^OfflineRegionCallback)(NSArray<CEOfflineRegion *> * _Nonnull packages);

@interface CEOfflinePackageManager : NSObject

/**
 * @name Offline Downloads
 *
 */

+ (nonnull CEOfflinePackageManager *)sharedInstance;

- (NSArray<CEOfflinePackage *> * _Nonnull)installedOfflinePackages;

- (void)setAPIBaseURL:(nonnull NSString *)url;

- (void)setSearchBaseURL:(nonnull NSString *)url;

- (void)availableCountries:(nullable OfflineRegionCallback)callback;

- (void)availableOfflinePackagesInCountry:(NSString * _Nonnull)countryCode limit:(NSInteger)limit offset:(NSInteger)offset callback:(nullable OfflinePackageCallback)callback;

- (void)nearbyPackages:(CLLocationCoordinate2D)location radius:(double)radius limit:(NSInteger)limit callback:(nullable OfflinePackageCallback)callback;

- (void)searchPackages:(nonnull NSString *)query limit:(NSInteger)limit callback:(nullable OfflinePackageCallback)callback;

- (void)packageForId:(nonnull NSString *)packageId callback:(nullable void(^)( CEOfflinePackage * _Nullable package))callback;

- (nullable CEOfflinePackage *)installedPackageForLocation:(CLLocationCoordinate2D)location;

- (nullable CEOfflinePackage *)installedPackageForId:(nonnull NSString *)packageId;

/** Will download the offline package, reporting progress of the download to delegate. */
- (nonnull CEOfflineDownload *)downloadOfflinePackage:(nonnull CEOfflinePackage *)package withDelegate:(nullable id<CEOfflineDownloadDelegate>)delegate;

/** Deletes an offline package. This will cancel any pending offline downloads. Common files such as iconography are not removed. */
- (void)removeOfflinePackage:(nonnull NSString *)package completion:(nullable void(^)(BOOL complete))completion;

/** Deletes all offline packages. All offline files are removed. */
- (void)removeAllOfflinePackages:(nullable void(^)(BOOL complete))completion;

/** Indicates whether a particular offline package is installed */
- (BOOL)packageInstalled:(nonnull NSString *)packageId;

- (nullable NSData *)findFile:(nonnull NSString *)filename;

- (nullable UIImage *)findImage:(nonnull NSString *)filename;

- (nullable UIImage *)findBusinessLogoWithLogoId:(NSInteger)logoId andCategoryIcon:(NSInteger)categoryIcon;

+ (NSInteger)version;

@end
