/** This abstract base class is used for layers which receive tiled data from external data sources.  In order to render a tile layer, a tile source must be provided 
 
 CETileLayer accepts the following options:
 
 - tileSource - A CETileSource object describing how this layer should load and render its data.
 - tileSize - The size of the tiles used by the tile source.
 - buffer - An optional buffer of tiles to load around the current screen.  Use this only if bandwidth and performance are not an issue.
 
 See CELayer for more available options.
 
 */

#import "CELayer.h"
#import "CEMapTypes.h"

@class CEDataSource;
@class CETile;

@interface CETileLayerOptions : CELayerOptions

- (id)init;
- (id)initWithDataSource:(CEDataSource *)dataSource;

@property (strong, nonatomic) CEDataSource *dataSource;
@property (assign, nonatomic) NSInteger tileSize;
@property (strong, nonatomic) NSMutableSet *levelOfDetailZoomLevels;

@end


/** Base class representing a layer on a CEMapView that is made up of tiles. */
@interface CETileLayer : CELayer

- (void)reloadTiles;
- (void)setLevelOfDetailZooms:(NSSet*)lodZooms;

/** The tile source used to provide the data for this layer */
@property (strong, nonatomic) CEDataSource *dataSource;

@property (assign, nonatomic, readonly) NSInteger tileSize;
@end
