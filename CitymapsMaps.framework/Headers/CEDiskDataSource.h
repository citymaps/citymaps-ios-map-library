#import <Foundation/Foundation.h>
#import "CEDataSource.h"

@interface CEDiskDataSourceOptions : CEDataSourceOptions

- (id) initWithFilepath:(NSString *)filepath;
@property (nonatomic, strong) NSString *filepath;

@end

/** A data source which receives its data from a local disk.
 
 The file path is determined using a token replacement techinque.  The tokens are described as follows:
 
 - {x} - The mercator x-coordinate of the tile
 - {y} - The mercator y-coordinate of the tile
 - {zoom} - The zoom level of the tile
 
 An example filepath:
 
 - /opt/data/tiles/{zoom}/{x}/{y}.png
 
 The following options are available at construction:
 
 - filePath - The tokenized file path for the data source.
 
 See CEDataSource for more information about data sources in general, and for additional available options.
 */

@interface CEDiskDataSource : CEDataSource

/** Initialize a new data source with a set of options
 
 @param options A dictionary of options used to initialize this data source.  Available options are:
 
 - maxCacheSize - The maximum size of the memory cache, measured in number of tiles.
 */

- (id)initWithOptions:(CEDataSourceOptions *)options;

- (id)initWithFilepath:(NSString *)filepath;

@property (nonatomic, strong) NSString *filepath;

@end
