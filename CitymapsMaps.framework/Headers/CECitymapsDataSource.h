#import <Foundation/Foundation.h>

#import "CEWebDataSource.h"

@interface CECitymapsDataSourceOptions : CEWebDataSourceOptions

@property (strong, nonatomic) NSString *offlinePattern;
@end

/** Abstract base class for Citymaps data sources. */
@interface CECitymapsDataSource : CEWebDataSource

- (id)initWithOptions:(CECitymapsDataSourceOptions *)option;
- (id)initWithURL:(NSString *)url andCacheVersion:(NSInteger)cacheVersion;

+ (CECitymapsDataSource*)defaultCitymapsMapDataSource;
+ (CECitymapsDataSource*)defaultCitymapsBuildingsDataSource;
+ (CECitymapsDataSource*)defaultBusinessDataSource;
+ (CECitymapsDataSource*)defaultRegionDataSource;

@end
