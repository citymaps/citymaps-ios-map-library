#import "CELayer.h"

@class CEFeatureGroup;
@class CEFeature;

/**
 * A layer to hold and display user defined shapes. These shapes are subclasses of CEFeature.
 */
@interface CECanvasLayer : CELayer

/**
 * @name Feature Group
 */

/** Gets the feature group with the specified name.  If it does not exist, it will be created.
 *
 * @param name The name of the feature group.
 */
- (CEFeatureGroup*)featureGroupWithName:(NSString*)name;

/**
 * @name Features
 */

/**
 * Adds a feature to the CanvasLayer
 * @param feature The feature to add to the CanvasLayer.
 */
- (void)addFeature:(CEFeature*)feature;

/**
 * Removes a feature from the CanvasLayer
 * @param feature The feature to remove from the CanvasLayer.
 */
- (void)removeFeature:(CEFeature*)feature;

/**
 * Removes all CEFeatures from the CanvasLayer
 */
- (void)removeAllFeatures;

@end
