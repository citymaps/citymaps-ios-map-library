//
//  CEDataTileLayer.h
//  MapEngineLibraryIOS
//
//  Created by Eddie Kimmel on 2/11/14.
//  Copyright (c) 2014 Adam Eskreis. All rights reserved.
//

#import "CETile.h"
#import "CETileLayer.h"

/** This delegate will indicate to the user when actions are taken on business markers in particular. */

typedef enum {
    kCEDataTileNotFound,
    kCEDataTileRefused,
    kCEDataTileTimedOut,
    kCEDataTileInternalServerError,
    kCEDataTileUnknownReason
} CEDataTileReason;

@class CEDataTileLayer;
@protocol CEDataTileLayerDelegate <NSObject>

/** Notifies the delegate when a tile has loaded
 @param layer - Layer the tile loaded for.
 @param gridPoint - The id of the tile.
 @param data - The data loaded for the tile.
 */
- (void)tileLayer:(CEDataTileLayer*)layer didLoadTile:(CETile*)tile withData:(NSData*) data;

/** Notifies the delegate when a tile has failed to load
 @param layer - Layer the tile loaded for.
 @param gridPoint - The id of the tile.
 @param reason - The reason given for the failure to load.
 */
- (void)tileLayer:(CEDataTileLayer*)layer failedToLoadTile:(CETile*)tile withReason:(CEDataTileReason) reason;

/** Notifies the delegate when a tile is unloaded from the map.
 @param layer - Layer the tile loaded for.
 @param gridPoint - The id of the tile.
 */
- (void)tileLayer:(CEDataTileLayer*)layer didRemoveTile:(CETile*)tile;

@end

/**
 * This tile layer allows the user to utilize the map to load in their own custom tiled data. This allows for paging of requests as the user zooms and pans around the map.
 */
@interface CEDataTileLayer : CETileLayer

/** The delegate to receive notifications about tile loading. */
@property (nonatomic, strong) id<CEDataTileLayerDelegate> delegate;

@end
