//
//  CEPolygon.h
//  vectormap2
//
//  Created by Eddie Kimmel on 8/14/13.
//  Copyright (c) 2013 Lion User. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CEMapTypes.h"
#import "CEFeature.h"

/** This class represents a user defined polygon to be placed on a CECanvasLayer.
 *
 *  The polygon should be non-self-intersecting. The holes should not go outside the bounds of the containing polygon.
 */
@interface CEPolygon : CEFeature

/**
 * @name Initializers
 */

/** Initializes a new polygon */
- (id)init;

/** Initializes a new polygon with the given points. 
 @param points Pointer to the list of points to initialize the polygon with.
 @param count the number of points to use out of the list of points.
 */
- (id)initWithPoints:(CLLocationCoordinate2D*)points count:(int)count;

/**
 * @name Points
 */

/**
 * Adds a point to this polygon.
 * @param point The point to add to the polygon
 */
- (void)addPoint:(CLLocationCoordinate2D)point;

/** This method adds a set of points to this polygon.
 * @param points Points to add to the polygon.
 * @param count number of points to add.
 */
- (void)addPoints:(CLLocationCoordinate2D*)points count:(int)count;

/**
 * @name Holes
 */

/**
 * Add a hole to this polygon. The hole should be completely contained within the polygon and should not be self-intersecting. The hole should not contain any holes itself.
 * @param hole The CEPolygon to add to this polygon as a hole.
 */
- (void)addHole:(CEPolygon *)hole;

/**
 * Removes a hole from this polygon.
 * @param hole The CEPolygon to remove from this polygon.
 */
- (void)removeHole:(CEPolygon *)hole;

/** Creates a new hole given the set of points and returns the CEPolygon used to represent the hole.
 * @param points Points to add to the polygon.
 * @param count number of points to add.
 */
- (CEPolygon*)addHole:(CLLocationCoordinate2D*)points count:(int)count;

/** Checks whether a coordinate is inside this polygon
 * @param coordinate The coordinate to check
 * @return Whether this coordinate is inside the polygon and not inside any of its holes
 */

- (BOOL)isCoordinateInside:(CLLocationCoordinate2D)coordinate;

@end
