//
//  CERoutingController.h
//  MapEngineLibraryIOS
//
//  Created by Adam Eskreis on 3/23/16.
//  Copyright © 2016 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CERoutingTypes.h"

@class CERoutingRequest;
@class CERoute;
@class CERouteState;

@interface CERoutingController : NSObject

+ (void)routeRequest:(CERoutingRequest *)request completion:(CERoutingCompletionBlock)completion;
+ (void)timeDistanceRequest:(CERoutingRequest *)request completion:(CETimeDistanceCompletionBlock)completion;

@end
