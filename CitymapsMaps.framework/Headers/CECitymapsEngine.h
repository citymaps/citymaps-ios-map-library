//
//  CECitymapsEngine.h
//  MapEngineLibraryIOS
//
//  Created by Edward Kimmel on 12/8/15.
//  Copyright © 2015 Adam Eskreis. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kCitymapsDidUpdateNotification;

@interface CECitymapsEngine : NSObject

+(void)initEngine;

@end
